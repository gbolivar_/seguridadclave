/**
 * @Author: gregoriobolivar.com.ve
 * @email: elalconxvii@gmail.com
 * @Creation Date: 28/12/2015
 * @Audited by: Gregorio J Bolívar B
 * @Modified Date: 28/12/2015
 * @Description: Codigo encargado de Validar el Nivel de Seguridad de la Clave.
 * @version: 0.2
 */
jQuery.fn.seguridadClave = function(){
	$(this).each(function(){
		// Patrones de Validacion
		var letMayus = /[A-Z]/;
		var letMinus = /[a-z]/;
		var paNumros = /[0-9]/;
		var paCarace = /[.,#$*!]/;

		var elem = $(this);
		var not  = $('<br><span class="notaMensa">La clave debe contener al menos una Mayúscula, una Minúscula, un Número y uno de estos Carácteres '+paCarace+'.</span>');
		msg = $('<span class="fortaleza">Escribe la clave</span>');
		elem.after(not)
		elem.after(msg)
		elem.data("mensaje", msg);
		elem.keyup(function(e){
			elem = $(this);
			msg = elem.data("mensaje")
			var claveActual = elem.val();
			var fortalezaActual = "";
			switch (claveActual.length){
				case 0:
				fortalezaActual = "Escribe la clave";
				break;
				case 1:
				case 2:
				case 3:
				case 4:
				fortalezaActual = "La clave debe contener al menos 5 caracteres";
				break;
				default:
				if((letMayus.test(claveActual))==true && (letMinus.test(claveActual))==false && (paNumros.test(claveActual))==false && (paCarace.test(claveActual))==false){
					fortalezaActual = "Clave no segura";
				}else if((letMayus.test(claveActual))==true && (letMinus.test(claveActual))==true && (paNumros.test(claveActual))==false && (paCarace.test(claveActual))==false){
					fortalezaActual = "Clave no segura";
				}else if((letMayus.test(claveActual))==true && (letMinus.test(claveActual))==true && (paNumros.test(claveActual))==true && (paCarace.test(claveActual))==false){
					fortalezaActual = "Clave medianamente segura";
				}else if((letMayus.test(claveActual))==true && (letMinus.test(claveActual))==true && (paNumros.test(claveActual))==true && (paCarace.test(claveActual))==true){
					fortalezaActual = "Clave segura";
				}else{
					fortalezaActual = "Clave no segura";
				}
				break;
			}
			msg.html(fortalezaActual);
		});
});
return this;
}